package main

import "testing"

func TestTallyImages(t *testing.T){
	testTally, errorTally := tallyImages("./")
	if errorTally != nil{
		t.Errorf("Returned err not null. err.Error() is: %s", errorTally.Error())
	}
	
	testImgs := []string{"1.jpg", "2.jpg", "3.jpg", "4.jpg"}

	if len(testTally) != 4{
		t.Errorf("Expected a slice with length of 4, got %d", len(testTally))
	}

	for i, img := range testTally{
		if img != testImgs[i] {
			t.Errorf("Incorrect image at index %d, expected %s, got %s", i, img, testImgs[i])
		}
	}
}

func TestImageProcessing(t *testing.T){
	
}

func TestWriteImageToFile(t *testing.T){
	// open sample pic from local dir
	// send to function to write it to local dir with name "random"
	// check if file exists on local disk
}