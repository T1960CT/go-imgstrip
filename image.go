package main

import (
	"fmt"
	"image"
	"os"
	"path/filepath"
	"strings"

	"github.com/anthonynsimon/bild/blend"
	"github.com/anthonynsimon/bild/clone"
	"github.com/anthonynsimon/bild/imgio"
	"github.com/anthonynsimon/bild/transform"
)

// Change these to set the per-image size.
// Image strip output size is:
// imgSizeY by (imgSizeX * # of images)
var imgSizeX, imgSizeY = 96, 96

// tallyImages will scan the directory path provided for image files,
// add their names to a slice (a list), and return the list of image names
func tallyImages(dir string) ([]string, error) {
	// Create slice to hold all files found
	var files []string

	// Collect all files in dir and append them to files list
	err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		files = append(files, path)
		return nil
	})
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	// Create slice to hold our img file names
	var f []string

	for _, file := range files {
		// Split file names from extensions so we only keep jpg or png files
		ext := strings.Split(file, ".")

		// Ensure file has an extension before checking index[1]
		if len(ext) > 1 {
			// Only append files that are jpg or png. If list becomes longer, we can use another loop, but overkill for 2 or 3 types.
			if ext[len(ext)-1] == "jpg" || ext[len(ext)-1] == "png" && ext[0] != "output" {
				// Add the file name to the f slice
				f = append(f, file)
			}
		}
	}

	// Return our slice of image files
	return f, nil
}

// imageProcessing will take a list of images, and the root directory,
// and manipulates all the images, and saves the final image strip
func imageProcessing(dir string, images []string) {
	// Create Image slice to hold our images for processing
	var c []image.Image

	// Loop over our image list. i = key, image = value, iterating over list images
	for i, image := range images {
		fmt.Println(dir + image)
		// Open current image and append it to our Image slice
		img, err := imgio.Open(dir + image)
		if err != nil {
			fmt.Println(err.Error())
			return
		}

		// Resize the image to standardize
		conformSize := transform.Resize(img, imgSizeX, imgSizeY, transform.Linear)

		// Pad the image with half the width of our total images on each side
		paddedX := clone.Pad(conformSize, imgSizeX*(len(images)-1)/2, 0, clone.NoFill)

		// Shift the image toward the left edge, offset by i number of images
		shiftedOver := transform.Translate(paddedX, -((imgSizeX * (len(images) - 1) / 2) - (imgSizeX * i)), 0)

		// Append the finished manipulation to the Image slice
		c = append(c, shiftedOver)
	}

	// Create final Image to write to file, setting it to our first Image layer
	var finalImage = c[0]

	// Loop over our stored Image slice
	for l := 1; l < len(c); l++ {
		// Overlay each subsequent Image layer to the previous iteration
		finalImage = blend.Add(finalImage, c[l])
	}

	// Send it to be stored as an image
	writeImageToDisk("output.png", finalImage, imgio.PNG)
}

// writeImageToDisk will take an Image object, and write the file to the local disk
func writeImageToDisk(filename string, img image.Image, format imgio.Format){
	// Write image to file
	if err := imgio.Save(filename, img, format); err != nil {
		fmt.Println(err.Error())
		return
	}
}
