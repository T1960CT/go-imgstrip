package main

/*
	This program will:
		scan given directory for images,
		add them into a list,
		iterate over the list,
		resize/standardize & stitch images into strip,
		save image strip to file

	Possible improvements:
		Image order, maybe by filename? Or if sent over API, by order listed in packet?
		Export multiple image size strips (viewport targetting?)
		Convert to micro-service with HTTP routes & POST/JSON payloads?
		Convert from base64 encoded images to facilitate transferring through API?
		Resize through different ways? Maybe provide 2 XY coordinates and crop (then resize?)
*/

func main() {
	images, _ := tallyImages("./")
	imageProcessing("./", images)
}